import express from "express";
import cors from "cors";
import db from "./config/databases.js";
import ProductRoutes from "./routes/products.js";
import UserRoutes from "./routes/users.js";
import OrderRoutes from "./routes/order.js";
import bodyParser from 'body-parser';
import multer from 'multer'

const form = multer();
const app = express();

app.use(bodyParser.json());
app.use(form.array());
app.use(bodyParser.urlencoded({
    extended: false
}));
try {
    await db.authenticate();
    console.log('Connection has been established successfully.');
} catch (error) {
    console.error('Unable to connect to the database:', error);
}
app.use(cors({
    credentials: true,
    origin: 'http://localhost:3000'
}));
app.use('/products', ProductRoutes);
app.use('/users', UserRoutes);
app.use('/orders', OrderRoutes);

export default app;