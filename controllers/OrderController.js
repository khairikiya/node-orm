import OrderDetail from '../models/order_detail.js';
import Order from '../models/order.js';
import {
    getUserId
} from '../middleware/cekAuth.js'
import order from '../models/order.js';


export const addOrder = async (req, res) => {
    const user_id = await getUserId(req, res);
    const countOrder = await Order.count();
    const order_code = 'OK' + String(countOrder).padStart(4, '0');

    Order.create({
        user_id: user_id,
        order_code: order_code,
        status: 1,
        createdAt: Date.now()
    }).then(result => {
        const order_id = result.order_id;
        var dataa = req.body;
        dataa.forEach(function (element) {
            element.status = "1";
            element.order_id = order_id;
        });

        console.log(dataa);

        OrderDetail.bulkCreate(dataa).then(result => {
            if (result) {
                return res.status(200).json({
                    "message": "Berhasil melakukan order"
                });
            } else {
                return res.status(403).json({
                    "message": "gagal melakukan order"
                });
            }
        })
    })
}

export const getOrderByOrderId = async (req, res) => {
    const order_id = req.params.order_id;
    Order.sequelize.query("select p.product_name, p.product_code, o.order_code, od.qty_order from orders o join order_details od on od.order_id = o.order_id left join products p on p.product_id = od.product_id where o.`status` = 1 and o.order_id = :order_id", {
        replacements: {
            order_id: order_id
        },
        type: Order.sequelize.QueryTypes.SELECT
    }).then(result => {
        res.status(200).json(result)
    })
}

export const getOrderByUser = async (req, res) => {
    const user_id = await getUserId(req, res);
    Order.sequelize.query("select o.order_id, o.order_code from orders o where o.`status` = 1 and o.user_id = :user_id", {
        replacements: {
            user_id: user_id
        },
        type: Order.sequelize.QueryTypes.SELECT
    }).then(result => {
        res.status(200).json(result)
    })
}

export const getAllOrder = async (req, res) => {

    Order.findAll({
        attributes: [
            'order_id',
            'order_code'
        ],
        where: {
            status: 1,

        }
    }).then(result => {
        return res.status(200).json(result);
    })
}