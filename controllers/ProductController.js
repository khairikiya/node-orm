import Product from "../models/products.js";
import {
    getUserId
} from '../middleware/cekAuth.js'
import {
    validationResult
} from "express-validator";
import order from "../models/order.js";
import {
    getOrderByUser
} from "./OrderController.js";


export const addProduct = async (req, res) => {

    try {
        const errors = validationResult(req).errors;
        if (errors.length > 0) {
            return res.status(400).json({
                error: errors
            })
        }
        //generate product code berdasarkan sequence
        const jmlProduk = await Product.count();
        const product_code = 'PK' + String(jmlProduk).padStart(4, '0');
        const user_id = await getUserId(req, res);

        const product = await Product.create({
            product_name: req.body.product_name,
            product_code: product_code,
            user_id: user_id,
            price: req.body.price,
            status: 1,
            createdAt: Date.now()
        });
        res.status(200).json(product);
    } catch (err) {
        console.log(err);
    }
}

export const getProductByUser = async (req, res) => {
    const user_id = await getUserId(req, res);

    const product = await Product.findAll({
        where: {
            status: 1,
            user_id: user_id
        }
    });
    return res.status(200).json(product);
}

export const getProductAll = async (req, res) => {

    const product = await Product.findAll({
        where: {
            status: 1
        }
    });
    return res.status(200).json(product);
}

export const getProductById = async (req, res) => {
    const product_id = req.params.product_id;

    const product = Product.sequelize.query(
        'SELECT p.product_name,p.price,u.NAME FROM products p LEFT JOIN users u ON u.user_id=p.user_id where  p.product_id = :product_id and p.status = :status', {
        replacements: {
            status: 1,
            product_id: product_id
        },
        type: Product.sequelize.QueryTypes.SELECT
    }
    ).then(result => {
        console.log(result)
        res.status(200).json(result[0]);
    })
}

export const updateProductById = async (req, res) => {

    console.log(req);
    const errors = await validationResult(req).errors;
    if (errors.length > 0) {
        return res.status(400).json({
            error: errors
        })
    }

    await Product.update({
        product_name: req.body.product_name,
        price: req.body.price
    }, {
        where: {
            product_id: req.body.product_id
        }
    }).then(result => {
        console.log(result)
        if (result > 0) {
            res.status(200).json({
                "message": "data berhasil di update"
            })
        } else {
            res.status(403).json({
                "message": "Gagal mengupdate data"
            })
        }
    })
}