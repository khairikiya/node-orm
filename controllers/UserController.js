import Users from "../models/users.js";
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import env from 'dotenv';
import {
    getUserId
} from '../middleware/cekAuth.js'
env.config();

import {
    validationResult
} from "express-validator";


export const getUsers = async (req, res) => {
    try {
        const user = await Users.findAll({
            attributes: [
                'user_id',
                'role_id',
                'name',
                'email',
                'password',
                'address',
                'phone'
            ],
            where: {
                status: 1
            }
        });
        if (user.length > 0) {
            res.status(200).json(user)
        } else {
            res.status(204).json({
                "message": "Data user kosong"
            });
        }
    } catch (err) {
        res.status(500).json(err);
    }
}

export const getUserById = async (req, res) => {
    try {
        const user = await Users.findAll({
            attributes: ['user_id',
                'email',
                'name',
                'address',
                'phone'
            ],
            where: {
                status: 1,
                user_id: req.params.user_id
            }
        });
        res.status(200).json(user);
    } catch (err) {
        res.status(500).json(err)
    }
}

export const addUser = async (req, res) => {
    console.log(req.body.email)
    const errors = validationResult(req).errors;
    if (errors.length > 0) {
        return res.status(400).json({
            error: errors
        })
    }


    const cekEmail = await Users.count({
        where: {
            email: req.body.email
        }
    })
    if (cekEmail > 0) {
        return res.status(406).json({
            'Message': 'Email telah digunakan'
        })
    } else {
        try {
            Users.create({
                role_id: 2,
                name: req.body.name,
                email: req.body.email,
                password: await bcrypt.hash(req.body.password, 10),
                address: req.body.address,
                phone: req.body.phone,
                created_at: Date.now(),
                status: 1

            }).then((result) => {
                res.status(200).json({
                    "name": result.name,
                    "email": result.email
                })
            })
        } catch (err) {
            res.status(505).json(err)
        }
    }
}

export const addAdmin = async (req, res) => {
    const errors = validationResult(req).errors;
    if (errors.length > 0) {
        return res.status(400).json({
            error: errors
        })
    }

    const cekEmail = await Users.count({
        where: {
            email: req.body.email
        }
    })
    if (cekEmail > 0) {
        return res.status(406).json({
            'Message': 'Email telah digunakan'
        })
    } else {
        try {
            Users.create({
                role_id: 1,
                name: req.body.name,
                email: req.body.email,
                password: await bcrypt.hash(req.body.password, 10),
                address: req.body.address,
                phone: req.body.phone,
                created_at: Date.now(),
                status: 1

            }).then((result) => {
                res.status(200).json({
                    "name": result.name,
                    "email": result.email
                })
            })
        } catch (err) {
            res.status(505).json(err)
        }
    }
}

export const loginUser = async (req, res) => {
    const cekAkun = await Users.findOne({

        where: {
            email: req.body.email
        }
    });
    console.log(cekAkun);

    if (cekAkun) {
        const validPassword = await bcrypt.compare(req.body.password, cekAkun.password);
        if (validPassword) {
            const dataToken = {
                user_id: cekAkun.user_id,
                name: cekAkun.name,
                email: cekAkun.email,
                role_id: cekAkun.role_id
            }
            const token = jwt.sign({
                    data: dataToken
                },
                process.env.TOKEN, {
                    expiresIn: "12hr"
                }
            )
            return res.status(200).json({
                user_id: cekAkun.user_id,
                name: cekAkun.name,
                email: cekAkun.email,
                token: token
            })

        } else {
            return res.status(401).json({
                'message': 'password salah'
            })
        }
    }
}


export const updateUser = async (req, res) => {

    const user_id = await getUserId(req, res);

    if (errors.length > 0) {
        return res.status(400).json({
            error: errors
        })
    }
    try {
        await Users.update({
            name: req.body.name,
            address: req.body.address,
            phone: req.body.phone,
            updatedAt: Date.now()

        }, {
            where: {
                user_id: user_id
            }
        });
        res.json({
            "message": "User Updated"
        });
    } catch (err) {
        console.log(err);
    }
}

export const getProfile = async (req, res) => {
    try {
        const user_id = await getUserId(req, res);

        console.log(user_id);
        const user = await Users.findOne({
            attributes: [
                'user_id',
                'name',
                'email',
                'phone'
            ],
            where: {
                user_id: user_id
            }
        });
        res.json(user);
    } catch (err) {
        console.log(err);
    }
}