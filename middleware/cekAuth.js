import jwt, {
    decode
} from 'jsonwebtoken';
import env from 'dotenv';

env.config();

export const cekAuthMember = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(" ")[1];
        console.log(token);
        const decoded = jwt.verify(token, process.env.TOKEN);

        if (decoded.data.role_id == 2) {
            res.userData = decoded;
            next();
        } else {
            console.log(decoded);
            return res.status(401).json({
                message: "Memerlukan akses member"
            })
        }
    } catch (error) {
        return res.status(401).json({
            message: "Auth Failed"
        })
    }
}

export const cekAuthAdmin = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(" ")[1];
        console.log(token);
        const decoded = jwt.verify(token, process.env.TOKEN);

        if (decoded.data.role_id == 1) {
            res.userData = decoded;
            next();
        } else {
            console.log(decoded);
            return res.status(401).json({
                message: "Memerlukan akses admin"
            })
        }

    } catch (error) {
        return res.status(401).json({
            message: "Auth Failed"
        })
    }
}

export const cekAuthAll = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(" ")[1];
        console.log(token);
        const decoded = jwt.verify(token, process.env.TOKEN);
        res.userData = decoded;
        next();

    } catch (error) {
        return res.status(401).json({
            message: "Auth Failed"
        })
    }
}

export const getUserId = async (req, res, next) => {
    try {
        const token = req.headers.authorization.split(" ")[1];
        console.log(token);
        const decoded = await jwt.verify(token, process.env.TOKEN);

        return decoded.data.user_id

    } catch (error) {
        return res.status(401).json({
            message: "Auth Failed"
        })
    }
}