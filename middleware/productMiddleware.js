import {
    check
} from "express-validator";

export const validationAddProduct = () => {
    return [
        check('product_name').notEmpty().withMessage('product name cannot be empty'),
        check('price').notEmpty().withMessage('price cannot be empty')
    ];
}