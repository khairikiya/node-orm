import {
    check
} from "express-validator";

export const registerValidator = () => {
    return [
        check('name').notEmpty().withMessage('name cannot be empty'),
        check('email').isEmail().withMessage('users not valid, please input valid user format (example@mail.com)')
    ]
}

export const updateUserValidator = () => {
    return [
        check('name').notEmpty().withMessage('name cannot be empty'),
        check('address').notEmpty().withMessage('address cannot be empty'),
        check('phone').notEmpty().isNumeric().withMessage('phone cannot be empty or must numeric format'),
    ]
}

export const loginValidator = () => {
    return [
        check('email').notEmpty.withMessage('Nama cannot be empty')
    ]
}