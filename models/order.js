// import sequelize 
import {
  Sequelize
} from "sequelize";
// import connection 
import db from "../config/databases.js";

// init DataTypes
const {
  DataTypes
} = Sequelize;

// Define schema
const order = db.define('orders', {

  // Define attributes
  order_id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  order_code: {
    type: DataTypes.STRING
  },
  status: {
    type: DataTypes.INTEGER
  },
  user_id: {
    type: DataTypes.INTEGER
  }
}, {
  // Freeze Table Name
  freezeTableName: true
});

export default order;