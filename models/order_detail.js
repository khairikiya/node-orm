// import sequelize 
import {
  Sequelize
} from "sequelize";
// import connection 
import db from "../config/databases.js";

// init DataTypes
const {
  DataTypes
} = Sequelize;

// Define schema
const order_detail = db.define('order_details', {

  // Define attributes
  order_detail_id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  qty_order: {
    type: DataTypes.INTEGER
  },
  status: {
    type: DataTypes.INTEGER
  },
  product_id: {
    type: DataTypes.INTEGER
  },
  order_id: {
    type: DataTypes.INTEGER
  }
}, {
  // Freeze Table Name
  freezeTableName: true
});

export default order_detail;