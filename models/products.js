// import sequelize 
import {
  Sequelize
} from "sequelize";
// import connection 
import db from "../config/databases.js";

// init DataTypes
const {
  DataTypes
} = Sequelize;

// Define schema
const products = db.define('products', {

  // Define attributes
  product_id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  product_code: {
    type: DataTypes.STRING
  },
  product_name: {
    type: DataTypes.STRING
  },
  price: {
    type: DataTypes.INTEGER
  },
  status: {
    type: DataTypes.INTEGER
  },
  user_id: {
    type: DataTypes.INTEGER
  }
}, {
  // Freeze Table Name
  freezeTableName: true
});

export default products;