// import sequelize 
import {
  Sequelize
} from "sequelize";
// import connection 
import db from "../config/databases.js";

// init DataTypes
const {
  DataTypes
} = Sequelize;

// Define schema
const users = db.define('users', {

  // Define attributes
  user_id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  role_id: {
    type: DataTypes.INTEGER
  },
  name: {
    type: DataTypes.STRING
  },
  email: {
    type: DataTypes.STRING
  },
  password: {
    type: DataTypes.STRING
  },
  address: {
    type: DataTypes.STRING
  },
  phone: {
    type: DataTypes.STRING
  },
  status: {
    type: DataTypes.INTEGER
  }
}, {
  // Freeze Table Name
  freezeTableName: true
});

export default users;