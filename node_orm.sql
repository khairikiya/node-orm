/*
 Navicat Premium Data Transfer

 Source Server         : Local
 Source Server Type    : MySQL
 Source Server Version : 100421
 Source Host           : localhost:3306
 Source Schema         : node_orm

 Target Server Type    : MySQL
 Target Server Version : 100421
 File Encoding         : 65001

 Date: 15/12/2021 11:08:58
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for order_details
-- ----------------------------
DROP TABLE IF EXISTS `order_details`;
CREATE TABLE `order_details`  (
  `order_detail_id` int NOT NULL AUTO_INCREMENT,
  `order_id` int NULL DEFAULT NULL,
  `product_id` int NULL DEFAULT NULL,
  `qty_order` int NULL DEFAULT NULL,
  `order_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` int NULL DEFAULT 1,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`order_detail_id`) USING BTREE,
  INDEX `order_id`(`order_id`) USING BTREE,
  INDEX `product_id`(`product_id`) USING BTREE,
  CONSTRAINT `order_details_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `order_details_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 41 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_details
-- ----------------------------
INSERT INTO `order_details` VALUES (37, 73, 73, 22, NULL, 1, '2021-12-15 03:39:01', '2021-12-15 03:39:01');
INSERT INTO `order_details` VALUES (38, 73, 76, 12, NULL, 1, '2021-12-15 03:39:01', '2021-12-15 03:39:01');
INSERT INTO `order_details` VALUES (39, 74, 73, 22, NULL, 1, '2021-12-15 03:40:38', '2021-12-15 03:40:38');
INSERT INTO `order_details` VALUES (40, 74, 76, 12, NULL, 1, '2021-12-15 03:40:38', '2021-12-15 03:40:38');

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `order_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NULL DEFAULT NULL,
  `order_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` int NULL DEFAULT 1,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`order_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 75 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES (1, 47, 'OK0000', 1, '2021-12-14 07:51:48', '2021-12-14 07:51:48');
INSERT INTO `orders` VALUES (2, 47, 'OK0001', 1, '2021-12-14 07:52:26', '2021-12-14 07:52:26');
INSERT INTO `orders` VALUES (3, 47, 'OK0002', 1, '2021-12-14 07:52:55', '2021-12-14 07:52:55');
INSERT INTO `orders` VALUES (4, 47, 'OK0003', 1, '2021-12-14 07:53:08', '2021-12-14 07:53:08');
INSERT INTO `orders` VALUES (5, 47, 'OK0004', 1, '2021-12-14 07:55:05', '2021-12-14 07:55:05');
INSERT INTO `orders` VALUES (6, 47, 'OK0005', 1, '2021-12-14 07:55:16', '2021-12-14 07:55:16');
INSERT INTO `orders` VALUES (7, 47, 'OK0006', 1, '2021-12-14 07:55:34', '2021-12-14 07:55:34');
INSERT INTO `orders` VALUES (8, 47, 'OK0007', 1, '2021-12-14 07:55:47', '2021-12-14 07:55:47');
INSERT INTO `orders` VALUES (9, 47, 'OK0008', 1, '2021-12-14 07:56:03', '2021-12-14 07:56:03');
INSERT INTO `orders` VALUES (10, 47, 'OK0009', 1, '2021-12-14 07:56:09', '2021-12-14 07:56:09');
INSERT INTO `orders` VALUES (11, 47, 'OK0010', 1, '2021-12-14 07:56:43', '2021-12-14 07:56:43');
INSERT INTO `orders` VALUES (12, 47, 'OK0011', 1, '2021-12-14 09:34:07', '2021-12-14 09:34:07');
INSERT INTO `orders` VALUES (13, 47, 'OK0012', 1, '2021-12-14 10:08:03', '2021-12-14 10:08:03');
INSERT INTO `orders` VALUES (14, 47, 'OK0013', 1, '2021-12-14 10:08:42', '2021-12-14 10:08:42');
INSERT INTO `orders` VALUES (15, 47, 'OK0014', 1, '2021-12-14 10:09:52', '2021-12-14 10:09:52');
INSERT INTO `orders` VALUES (16, 47, 'OK0015', 1, '2021-12-14 10:10:03', '2021-12-14 10:10:03');
INSERT INTO `orders` VALUES (17, 47, 'OK0016', 1, '2021-12-14 10:12:21', '2021-12-14 10:12:21');
INSERT INTO `orders` VALUES (18, 47, 'OK0017', 1, '2021-12-14 10:13:33', '2021-12-14 10:13:33');
INSERT INTO `orders` VALUES (19, 47, 'OK0018', 1, '2021-12-14 10:14:34', '2021-12-14 10:14:34');
INSERT INTO `orders` VALUES (20, 47, 'OK0019', 1, '2021-12-14 10:14:52', '2021-12-14 10:14:52');
INSERT INTO `orders` VALUES (21, 47, 'OK0020', 1, '2021-12-14 10:17:13', '2021-12-14 10:17:13');
INSERT INTO `orders` VALUES (22, 47, 'OK0021', 1, '2021-12-14 10:17:22', '2021-12-14 10:17:22');
INSERT INTO `orders` VALUES (23, 47, 'OK0022', 1, '2021-12-14 10:19:01', '2021-12-14 10:19:01');
INSERT INTO `orders` VALUES (24, 47, 'OK0023', 1, '2021-12-14 10:20:07', '2021-12-14 10:20:07');
INSERT INTO `orders` VALUES (25, 47, 'OK0024', 1, '2021-12-14 10:20:32', '2021-12-14 10:20:32');
INSERT INTO `orders` VALUES (26, 47, 'OK0025', 1, '2021-12-14 10:22:08', '2021-12-14 10:22:08');
INSERT INTO `orders` VALUES (27, 47, 'OK0026', 1, '2021-12-14 10:22:22', '2021-12-14 10:22:22');
INSERT INTO `orders` VALUES (28, 47, 'OK0027', 1, '2021-12-14 10:24:42', '2021-12-14 10:24:42');
INSERT INTO `orders` VALUES (29, 47, 'OK0028', 1, '2021-12-14 10:26:19', '2021-12-14 10:26:19');
INSERT INTO `orders` VALUES (30, 47, 'OK0029', 1, '2021-12-14 10:26:31', '2021-12-14 10:26:31');
INSERT INTO `orders` VALUES (31, 47, 'OK0030', 1, '2021-12-14 10:56:20', '2021-12-14 10:56:20');
INSERT INTO `orders` VALUES (32, 47, 'OK0031', 1, '2021-12-14 10:56:27', '2021-12-14 10:56:27');
INSERT INTO `orders` VALUES (33, 47, 'OK0032', 1, '2021-12-14 11:00:00', '2021-12-14 11:00:00');
INSERT INTO `orders` VALUES (34, 47, 'OK0033', 1, '2021-12-14 11:00:44', '2021-12-14 11:00:44');
INSERT INTO `orders` VALUES (35, 47, 'OK0034', 1, '2021-12-14 11:02:12', '2021-12-14 11:02:12');
INSERT INTO `orders` VALUES (36, 47, 'OK0035', 1, '2021-12-14 11:03:21', '2021-12-14 11:03:21');
INSERT INTO `orders` VALUES (37, 47, 'OK0036', 1, '2021-12-14 11:03:58', '2021-12-14 11:03:58');
INSERT INTO `orders` VALUES (38, 47, 'OK0037', 1, '2021-12-14 11:04:24', '2021-12-14 11:04:24');
INSERT INTO `orders` VALUES (39, 47, 'OK0038', 1, '2021-12-14 11:05:59', '2021-12-14 11:05:59');
INSERT INTO `orders` VALUES (40, 47, 'OK0039', 1, '2021-12-15 02:12:26', '2021-12-15 02:12:26');
INSERT INTO `orders` VALUES (41, 47, 'OK0040', 1, '2021-12-15 02:12:52', '2021-12-15 02:12:52');
INSERT INTO `orders` VALUES (42, 47, 'OK0041', 1, '2021-12-15 02:19:20', '2021-12-15 02:19:20');
INSERT INTO `orders` VALUES (43, 47, 'OK0042', 1, '2021-12-15 02:19:50', '2021-12-15 02:19:50');
INSERT INTO `orders` VALUES (44, 47, 'OK0043', 1, '2021-12-15 02:20:41', '2021-12-15 02:20:41');
INSERT INTO `orders` VALUES (45, 47, 'OK0044', 1, '2021-12-15 02:34:31', '2021-12-15 02:34:31');
INSERT INTO `orders` VALUES (46, 47, 'OK0045', 1, '2021-12-15 02:35:09', '2021-12-15 02:35:09');
INSERT INTO `orders` VALUES (47, 47, 'OK0046', 1, '2021-12-15 03:18:54', '2021-12-15 03:18:54');
INSERT INTO `orders` VALUES (48, 47, 'OK0047', 1, '2021-12-15 03:19:04', '2021-12-15 03:19:04');
INSERT INTO `orders` VALUES (49, 47, 'OK0048', 1, '2021-12-15 03:20:46', '2021-12-15 03:20:46');
INSERT INTO `orders` VALUES (50, 47, 'OK0049', 1, '2021-12-15 03:20:52', '2021-12-15 03:20:52');
INSERT INTO `orders` VALUES (51, 47, 'OK0050', 1, '2021-12-15 03:21:05', '2021-12-15 03:21:05');
INSERT INTO `orders` VALUES (52, 47, 'OK0051', 1, '2021-12-15 03:21:23', '2021-12-15 03:21:23');
INSERT INTO `orders` VALUES (53, 47, 'OK0052', 1, '2021-12-15 03:21:30', '2021-12-15 03:21:30');
INSERT INTO `orders` VALUES (54, 47, 'OK0053', 1, '2021-12-15 03:22:21', '2021-12-15 03:22:21');
INSERT INTO `orders` VALUES (55, 47, 'OK0054', 1, '2021-12-15 03:26:02', '2021-12-15 03:26:02');
INSERT INTO `orders` VALUES (56, 47, 'OK0055', 1, '2021-12-15 03:26:20', '2021-12-15 03:26:20');
INSERT INTO `orders` VALUES (57, 47, 'OK0056', 1, '2021-12-15 03:26:50', '2021-12-15 03:26:50');
INSERT INTO `orders` VALUES (58, 47, 'OK0057', 1, '2021-12-15 03:27:03', '2021-12-15 03:27:03');
INSERT INTO `orders` VALUES (59, 47, 'OK0058', 1, '2021-12-15 03:27:14', '2021-12-15 03:27:14');
INSERT INTO `orders` VALUES (60, 47, 'OK0059', 1, '2021-12-15 03:27:33', '2021-12-15 03:27:33');
INSERT INTO `orders` VALUES (61, 47, 'OK0060', 1, '2021-12-15 03:27:43', '2021-12-15 03:27:43');
INSERT INTO `orders` VALUES (62, 47, 'OK0061', 1, '2021-12-15 03:27:53', '2021-12-15 03:27:53');
INSERT INTO `orders` VALUES (63, 47, 'OK0062', 1, '2021-12-15 03:28:20', '2021-12-15 03:28:20');
INSERT INTO `orders` VALUES (64, 47, 'OK0063', 1, '2021-12-15 03:29:55', '2021-12-15 03:29:55');
INSERT INTO `orders` VALUES (65, 47, 'OK0064', 1, '2021-12-15 03:30:12', '2021-12-15 03:30:12');
INSERT INTO `orders` VALUES (66, 47, 'OK0065', 1, '2021-12-15 03:31:39', '2021-12-15 03:31:39');
INSERT INTO `orders` VALUES (67, 47, 'OK0066', 1, '2021-12-15 03:32:52', '2021-12-15 03:32:52');
INSERT INTO `orders` VALUES (68, 47, 'OK0067', 1, '2021-12-15 03:33:28', '2021-12-15 03:33:28');
INSERT INTO `orders` VALUES (69, 47, 'OK0068', 1, '2021-12-15 03:35:04', '2021-12-15 03:35:04');
INSERT INTO `orders` VALUES (70, 47, 'OK0069', 1, '2021-12-15 03:35:37', '2021-12-15 03:35:37');
INSERT INTO `orders` VALUES (71, 47, 'OK0070', 1, '2021-12-15 03:35:46', '2021-12-15 03:35:46');
INSERT INTO `orders` VALUES (72, 47, 'OK0071', 1, '2021-12-15 03:38:46', '2021-12-15 03:38:46');
INSERT INTO `orders` VALUES (73, 47, 'OK0072', 1, '2021-12-15 03:39:01', '2021-12-15 03:39:01');
INSERT INTO `orders` VALUES (74, 47, 'OK0073', 1, '2021-12-15 03:40:37', '2021-12-15 03:40:37');

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products`  (
  `product_id` int NOT NULL AUTO_INCREMENT,
  `product_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `user_id` int NULL DEFAULT NULL,
  `product_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `price` int NULL DEFAULT NULL,
  `status` int NULL DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`product_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  CONSTRAINT `products_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 81 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES (72, 'PK0000', 46, 'Samsung Galaxy J2', 1800000, 1, '2021-12-14 04:55:37', '2021-12-14 04:55:37');
INSERT INTO `products` VALUES (73, 'PK0001', 46, 'Samsung Galaxy S7', 900000, 1, '2021-12-14 04:57:04', '2021-12-14 04:57:04');
INSERT INTO `products` VALUES (74, 'PK0002', 46, 'Samsung Galaxy A32', 900000, 1, '2021-12-14 04:57:10', '2021-12-14 04:57:10');
INSERT INTO `products` VALUES (75, 'PK0003', 46, 'Toshiba Satelite', 4000, 1, '2021-12-14 04:57:37', '2021-12-14 07:30:23');
INSERT INTO `products` VALUES (76, 'PK0004', 46, 'Redmi Note 9', 19800000, 1, '2021-12-14 04:57:50', '2021-12-14 04:57:50');
INSERT INTO `products` VALUES (77, 'PK0005', 46, 'Note 20 Ultra', 4000, 1, '2021-12-14 04:59:07', '2021-12-14 07:29:30');
INSERT INTO `products` VALUES (78, 'PK0006', 46, 'Oppo', 4500000, 1, '2021-12-14 07:00:33', '2021-12-14 07:00:33');
INSERT INTO `products` VALUES (79, 'PK0007', 46, 'Oppo A32', 4500000, 1, '2021-12-14 07:04:56', '2021-12-14 07:04:56');
INSERT INTO `products` VALUES (80, 'PK0008', 46, 'Oppo Aqua', 20000, 1, '2021-12-14 07:20:43', '2021-12-14 07:20:43');

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `role_id` int NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` int NULL DEFAULT 1,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES (1, 'Admin', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `roles` VALUES (2, 'Member', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for sequelizemeta
-- ----------------------------
DROP TABLE IF EXISTS `sequelizemeta`;
CREATE TABLE `sequelizemeta`  (
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`name`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sequelizemeta
-- ----------------------------
INSERT INTO `sequelizemeta` VALUES ('20211208024131-create-roles.js');
INSERT INTO `sequelizemeta` VALUES ('20211209074208-create-users.js');
INSERT INTO `sequelizemeta` VALUES ('20211209074547-create-products.js');
INSERT INTO `sequelizemeta` VALUES ('20211210032041-create-order.js');
INSERT INTO `sequelizemeta` VALUES ('20211210032317-create-order-detail.js');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `role_id` int NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` int NULL DEFAULT 0,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`user_id`) USING BTREE,
  INDEX `role_id`(`role_id`) USING BTREE,
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 48 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (46, 1, 'Hidayatul Khairi', 'skiade07@gmail.com', '$2b$10$BYxuGGxkBoOrsO3JNUCrru.RyQPKaeUxiWctP5gJPxQxyyUpC6JFO', 'Jl. Anggrek Cendrawasih', '08128282828', 1, '2021-12-14 03:07:52', '2021-12-14 03:07:52');
INSERT INTO `users` VALUES (47, 2, 'Member ORMA', 'khairikiya@gmail.com', '$2b$10$DuYavajRecelVv5OBndzx.BMlAhzc5EuzkEHSkV7FzmAoAvAomEPW', 'Jl. Xiaomi KM 2', '091280128', 1, '2021-12-14 03:08:15', '2021-12-14 07:17:24');

SET FOREIGN_KEY_CHECKS = 1;
