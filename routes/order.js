import express from "express";

import {
    addOrder,
    getAllOrder,
    getOrderByOrderId,
    getOrderByUser

} from "../controllers/OrderController.js";

import {
    cekAuthAdmin,
    cekAuthAll,
    cekAuthMember
} from "../middleware/cekAuth.js";

const router = express.Router();

router.post('/', [cekAuthMember], addOrder);
router.get('/getOrder/:order_id', [cekAuthMember], getOrderByOrderId);
router.get('/listOrders', [cekAuthMember], getOrderByUser);
router.get('/orderAll', [cekAuthAdmin], getAllOrder);


export default router;