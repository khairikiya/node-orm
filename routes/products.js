import express from "express";

import {
    addProduct,
    getProductAll,
    getProductById,
    getProductByUser,
    updateProductById
} from "../controllers/ProductController.js";

import {
    cekAuthAdmin,
    cekAuthAll
} from "../middleware/cekAuth.js";

import {
    validationAddProduct
} from "../middleware/productMiddleware.js";

const router = express.Router();

router.post('/', [cekAuthAdmin, validationAddProduct()], addProduct);
router.get('/admin/', [cekAuthAll], getProductByUser);
router.get('/', [cekAuthAll], getProductAll);
router.get('/:product_id', [cekAuthAll], getProductById);
router.put('/update', [cekAuthAdmin, validationAddProduct()], updateProductById);

export default router;