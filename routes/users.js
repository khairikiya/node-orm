import express from "express";
import {
    registerValidator,
    updateUserValidator
} from "../middleware/userMiddleware.js";
import {
    getUsers,
    getUserById,
    addUser,
    loginUser,
    addAdmin,
    updateUser,
    getProfile
} from "../controllers/UserController.js";

import {
    cekAuthAdmin,
    cekAuthAll,
    cekAuthMember
} from '../middleware/cekAuth.js';


const router = express.Router();
router.get('/', cekAuthAdmin, getUsers);
router.get('/cari/:user_id', cekAuthAdmin, getUserById);
router.get('/getInfo', cekAuthAll, getProfile);
router.post('/', registerValidator(), addUser);
router.post('/addAdmin', registerValidator(), addAdmin);
router.post('/loginUser', loginUser);
router.put('/', [cekAuthAll, updateUserValidator()], updateUser);

export default router;