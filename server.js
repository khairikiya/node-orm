import http from 'http';
import app from './app.js';
app.listen(5000, () => console.log('Server running at http://localhost:5000'));
const server = http.createServer(app);
// listen on port
server.listen();